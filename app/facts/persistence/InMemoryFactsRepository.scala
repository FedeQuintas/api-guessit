package facts.persistence

import facts.domain.{CharacterFacts, FactsRepository}

import scala.concurrent.Future

class InMemoryFactsRepository() extends FactsRepository {

  private val facts = Array(CharacterFacts(1, Array("a", "b", "c"), "Hugo Moyano"), CharacterFacts(2, Array("d", "e", "f"), "Juanjo Buscaglia"),
    CharacterFacts(3, Array("1", "2", "3"), "El bati"), CharacterFacts(4, Array("Yd", "Ye", "Yf"), "El Rolo"),
    CharacterFacts(5, Array("1a", "1b", "1c"), "Hugo Toresani"), CharacterFacts(6, Array("Y1d", "Y1e", "Y1f"), "Juanjo"),
    CharacterFacts(7, Array("2a", "2b", "2c"), "El huevo"), CharacterFacts(8, Array("Y2d", "Y2e", "Y2f"), "Buscaglia"),
    CharacterFacts(9, Array("3a", "3b", "3c"), "Guevara"), CharacterFacts(10, Array("d", "e", "f"), "Juanes"),
    CharacterFacts(11, Array("4a", "4b", "4c"), "Ernesto"), CharacterFacts(12, Array("d", "e", "f"), "Yaque"),
    CharacterFacts(13, Array("5a", "5b", "5c"), "El che"), CharacterFacts(14, Array("d", "e", "f"), "El beto"),
    CharacterFacts(15, Array("6a", "6b", "6c"), "coslovaquia"), CharacterFacts(16, Array("d", "e", "f"), "Alonso"),
    CharacterFacts(17, Array("7a", "7b", "7c"), "carito"), CharacterFacts(18, Array("d", "e", "f"), "Fernando"))

  private var nextIdForUser : Map[Int, Int] = Map(1 -> 1, 2 -> 1)

  override def findById(id: Int): Future[CharacterFacts] = Future.successful(facts.filter(char => char.characterId == id).head)

  override def nextIdForUser(userId: Int): Future[Int] = {
    nextIdForUser.get(userId) match {
      case nextId: Some[Int] => {
        nextIdForUser = nextIdForUser.updated(userId, 1 + nextId.get)
        Future.successful(nextId.get)
      }
      case None => throw new RuntimeException("User does not exist")
    }

  }
}
