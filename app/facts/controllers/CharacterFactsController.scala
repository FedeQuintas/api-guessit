package facts.controllers

import javax.inject.{Inject, Singleton}

import facts.actions.{FactsResponse, GetFactsForUser}
import play.api.libs.json.{Json, Writes}
import play.api.mvc.{Action, Controller}

import scala.concurrent.ExecutionContext.Implicits.global

@Singleton
class CharacterFactsController @Inject()(val getFactsForUserAction: GetFactsForUser) extends Controller {

  private implicit val factWrites = new Writes[FactsResponse] {
    def writes(response: FactsResponse) = Json.obj(
      "id" -> response.characterFact.characterId,
      "facts" -> response.characterFact.facts,
      "character" -> response.characterFact.character,
      "score" -> response.score
    )
  }

  def getFactsForUser(id: Int) = Action.async {
    getFactsForUserAction.getFacts(id).map{ factsResponse =>
      factsResponse match {
        case Right(response) => Ok(Json.obj("result" -> "Correct", "characterFacts" -> Json.toJson(response)))
        case Left(response) => Ok(response)
      }
    }
  }

}
