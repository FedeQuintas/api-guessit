package facts.controllers

import javax.inject.{Inject, Singleton}

import facts.actions.{FactsResponse, GetFactsForUser, GuessCharacter}
import play.api.libs.json.{JsValue, Json, Writes}
import play.api.mvc.{Action, Controller}

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future


@Singleton
class GuessController @Inject()(val guessCharacter: GuessCharacter, val getFactsForUser: GetFactsForUser) extends Controller {

  private implicit val factWrites = new Writes[FactsResponse] {
    def writes(response: FactsResponse) = Json.obj(
      "id" -> response.characterFact.characterId,
      "facts" -> response.characterFact.facts,
      "character" -> response.characterFact.character,
      "score" -> response.score
    )
  }

  def guess(): Action[JsValue] = Action.async(parse.json) { request =>

    val answer = (request.body \ "answer").as[String]
    val characterId = (request.body \ "character_id").as[Int]
    val userId = (request.body \ "user_id").as[Int] // Can be in the url

    guessCharacter.guess(answer, characterId, userId).flatMap{ result =>
      if(result == "Correct") {
        getFactsForUser.getFacts(userId).map(factsResponse =>{
          factsResponse match {
            case Right(response) => Ok(Json.obj("result" -> "Correct", "characterFacts" -> Json.toJson(response)))
            case Left(response) => Ok(response)
          }
        })
      } else {
        Future.successful(Ok(Json.obj("result" -> "Incorrect")))
      }
    }

  }

}
