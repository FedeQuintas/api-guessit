package facts.domain

import scala.concurrent.Future

trait FactsRepository {

  def nextIdForUser(userId: Int): Future[Int]

  def findById(id: Int): Future[CharacterFacts]
}
