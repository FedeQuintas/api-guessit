package facts.actions

import javax.inject.Inject

import facts.domain.FactsRepository
import users.persistence.UserRepository

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future


class GuessCharacter @Inject()(val factsRepository: FactsRepository, val usersRepository: UserRepository) {

  def guess(answer: String, characterId: Int, userId: Int): Future[String] = {

    factsRepository.findById(characterId).map{ characterFacts =>
       if(characterFacts.character.toLowerCase() != answer.toLowerCase) {
         "Incorrect"
       } else {
         usersRepository.addPointToUser(userId)
        "Correct"
      }
    }
  }
}
