package facts.actions

import javax.inject.Inject

import facts.domain.{CharacterFacts, FactsRepository}
import users.persistence.UserRepository

import scala.concurrent.ExecutionContext.Implicits.global
import scala.concurrent.Future


case class FactsResponse(characterFact: CharacterFacts, score: Int)


class GetFactsForUser @Inject()(val factsRepository: FactsRepository, val usersRepository: UserRepository) {

  def getFacts(userId: Int): Future[Either[String, FactsResponse]] = {

    factsRepository.nextIdForUser(userId).flatMap { nextId =>
      val findByIdFuture = factsRepository.findById(nextId)
      val getUserScoreFuture = usersRepository.getScoreById(userId)

      val factsAndScore = for {
        facts <- findByIdFuture
        userScore <- getUserScoreFuture
      } yield (facts, userScore)

      factsAndScore.map( result => {
        result._2 match {
            case Some(score) => Right(FactsResponse(result._1, score.toInt))
            case None => Left("User not found")
          }
        }
      )
    }

  }

}
