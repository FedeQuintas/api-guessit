import javax.inject.Inject

import play.api.http.DefaultHttpFilters
import play.filters.hosts.AllowedHostsFilter


/**
 * Add the following filters by default to all projects
 * 
 * https://www.playframework.com/documentation/latest/ScalaCsrf 
 * https://www.playframework.com/documentation/latest/AllowedHostsFilter
 * https://www.playframework.com/documentation/latest/SecurityHeaders
 */
class Filters @Inject()(
  allowedHostsFilter: AllowedHostsFilter
) extends DefaultHttpFilters(
  allowedHostsFilter
)

/*{

  override val filters = Seq(csrfFilter, allowedHostsFilter, securityHeadersFilter, loggingFilter)
}*/
