package config

import com.google.inject.AbstractModule
import facts.domain.FactsRepository
import facts.persistence.InMemoryFactsRepository
import users.persistence.{AWSUserRepository, UserRepository}

class DependenciesConfiguration extends AbstractModule {
  def configure() = {
    bind(classOf[FactsRepository])
      .to(classOf[InMemoryFactsRepository])
    bind(classOf[UserRepository])
      .to(classOf[AWSUserRepository])
  }
}
