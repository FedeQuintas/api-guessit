package users.persistence

import scala.concurrent.Future

trait UserRepository {

  def addPointToUser(userId: Int): Future[Unit]

  def getScoreById(userId: Int): Future[Option[String]]
}
