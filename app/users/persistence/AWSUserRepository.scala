package users.persistence

import javax.inject.Singleton

import com.amazonaws.client.builder.AwsClientBuilder.EndpointConfiguration
import com.amazonaws.services.dynamodbv2.AmazonDynamoDBClientBuilder
import com.gu.scanamo._
import com.gu.scanamo.query.{KeyEquals, Query, UniqueKey}
import com.gu.scanamo.syntax._

import scala.concurrent.Future


case class UserScore(userID: Long, score: Int)

@Singleton
class AWSUserRepository extends UserRepository {

  val users = Table[UserScore]("UserScore")

  val client = {
    val conf = new EndpointConfiguration("http://localhost:8000", "us-west-2")
    AmazonDynamoDBClientBuilder.standard().withEndpointConfiguration(conf).build()
  }

   def addPointToUser(userId: Int): Future[Unit] = {
    Future.successful(Scanamo.exec(client)(users.query(Query(KeyEquals('userID, userId)))).map { response =>
      if (response.isRight){
        val newScore = response.right.get.score + 1
        Scanamo.exec(client)(users.update(UniqueKey(KeyEquals('userID, userId)), set('score -> newScore )))
      } else {
        Scanamo.exec(client)(users.put(UserScore(userId , 1)))
      }
    })
  }

  def getScoreById(userId: Int): Future[Option[String]] = {
    Future.successful{
      Scanamo.exec(client)(users.query(Query(KeyEquals('userID, userId)))).map { response =>
        response match {
          case Left(_) => "DynamoDB Error"
          case Right(_) => response.right.get.score.toString
        }
      }.headOption
    }
  }

}
