package users.controllers

import javax.inject.Inject

import play.api.mvc.{Action, Controller}
import users.persistence.AWSUserRepository

import scala.concurrent.ExecutionContext.Implicits.global


class UserController @Inject()(val userRepository: AWSUserRepository) extends Controller {

  def getScoreForUser(id: Int) = Action.async {
    userRepository.getScoreById(id).map{ response =>
      response match {
        case Some(score) =>Ok(score)
        case None => Ok("User not found")
      }
    }
  }

}
