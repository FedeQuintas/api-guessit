

name := """robiew"""

organization := "com."

version := "1.0-SNAPSHOT"

scalaVersion := "2.11.11"

lazy val root = (project in file(".")).enablePlugins(PlayScala)

resolvers += Resolver.sonatypeRepo("snapshots")

libraryDependencies += filters

libraryDependencies += "org.scalatestplus.play" %% "scalatestplus-play" % "2.0.0" % Test

libraryDependencies += "org.scalamock" %% "scalamock-scalatest-support" % "3.5.0" % Test

libraryDependencies += "org.mockito" % "mockito-all" % "1.8.4"

libraryDependencies ++= Seq(
"com.typesafe.play" % "play-cache_2.11" % "2.5.4"
)

libraryDependencies ++= Seq(
  "com.gu" %% "scanamo" % "1.0.0-M6"
)

resolvers ++= Seq(Resolver.mavenLocal, "Sonatype snapshots repository" at "https://oss.sonatype.org/content/repositories/snapshots/")

