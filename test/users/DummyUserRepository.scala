package users

import users.persistence.UserRepository

import scala.concurrent.Future

case class DummyUserRepository() extends UserRepository {

  var wasCalled: Boolean = false

  override def addPointToUser(userId: Int): Future[Unit] = Future.successful{
    wasCalled = true
    ()
  }

  override def getScoreById(userId: Int): Future[Option[String]] = Future.successful(Some("0"))

}
