package facts.actions

import facts.persistence.InMemoryFactsRepository
import org.scalatest.BeforeAndAfterEach
import org.scalatest.concurrent.ScalaFutures
import org.scalatestplus.play.PlaySpec
import users.DummyUserRepository

class GetFactsForUserSpec extends PlaySpec with ScalaFutures with BeforeAndAfterEach{

  private val userId = 1

  private var getFactsForUser : GetFactsForUser = _

  override def beforeEach() {
    getFactsForUser = new GetFactsForUser(new InMemoryFactsRepository(), new DummyUserRepository())
  }


  "GetFactsForUser" should {

    "retrieve first Character and user Score when a user asks for the first time" in {
      whenReady(getFactsForUser.getFacts(userId)) { result =>
        result.right.get.score must be(0)
        result.right.get.characterFact.characterId must be(1)
      }
    }

    "retrieve second Character when a user asks for the second time" in {
      whenReady(getFactsForUser.getFacts(userId)) { _ =>
        whenReady(getFactsForUser.getFacts(userId)) { result =>
          result.right.get.characterFact.characterId must be(2)
        }
      }
    }

    "retrieve first Character for each user when two different users ask for the first time" in {
      whenReady(getFactsForUser.getFacts(userId)) { result =>
        result.right.get.score must be(0)
        result.right.get.characterFact.characterId must be(1)
      }
      whenReady(getFactsForUser.getFacts(userId + 1)) { result =>
        result.right.get.score must be(0)
        result.right.get.characterFact.characterId must be(1)
      }
    }

    "throws an exception when user does not exist" in {
      val ex = intercept[RuntimeException] {
        getFactsForUser.getFacts(3)
      }
      ex.getMessage must be("User does not exist")
    }
  }

}
