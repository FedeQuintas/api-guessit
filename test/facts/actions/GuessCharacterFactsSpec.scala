package facts.actions

import facts.persistence.InMemoryFactsRepository
import org.scalamock.scalatest.MockFactory
import org.scalatest.concurrent.ScalaFutures
import org.scalatestplus.play.PlaySpec
import users.DummyUserRepository

class GuessCharacterFactsSpec extends PlaySpec with ScalaFutures with MockFactory {

  val userId = 100
  val characterId = 1

  val dummyUserRepository = new DummyUserRepository()

  val guessCharacter = new GuessCharacter(new InMemoryFactsRepository(), dummyUserRepository)


  "GuessCharacterFactsSpec" should {

    "retrieves Correct and updates score when a user answers correctly" in {

      whenReady(guessCharacter.guess("Hugo Moyano", characterId, userId)) { guessResult =>
        dummyUserRepository.wasCalled must be (true)
        guessResult must be("Correct")
      }

    }

    "retrieves Incorrect when a user answers incorrectly" in {

      whenReady(guessCharacter.guess("Rolo", characterId, userId)) { guessResult =>
        guessResult must be("Incorrect")
      }

    }

  }

}
